**Actualización**: Hemos movido las lecciones a [GitHub](https://github.com/franktoffel/CFDPython-ES) tal y como hizo el repositorio original. Las contribuciones nuevas serán aceptadas allí :)

**Update**: We moved the lessons to a [GitHub repository](https://github.com/franktoffel/CFDPython-ES) as the original did. 

***

Bievenido al repositorio online de mecánica de fluidos computacional ([CFD](http://es.wikipedia.org/wiki/Mec%C3%A1nica_de_fluidos_computacional))

Estos notebooks forman parte del primer módulo interactivo online de [CFD con Python](https://github.com/barbagroup/CFDPython) impartido por la profesora [Lorena A. Barba](http://lorenabarba.com/) y llamado **12 Steps to Navier-Stokes.**.

Texto y código sujeto bajo Creative Commons Attribution license, CC-BY-SA. (c) Original por Lorena A. Barba y Gilbert Forsyth en 2013, traducido por F.J. Navarro-Brull para [CAChemE.org](http://www.cacheme.org/)

[@LorenaABarba](https://twitter.com/LorenaABarba) - 
[@CAChemEorg](https://twitter.com/cachemeorg)



Lecciones
-------

* [Introduccion rapida a Python](http://nbviewer.ipython.org/urls/bitbucket.org/franktoffel/cfd-python-class-es/raw/master/lecciones/00%20-%20Introduccion%20rapida%20a%20Python.ipynb)
* [Paso 1](http://nbviewer.ipython.org/urls/bitbucket.org/franktoffel/cfd-python-class-es/raw/master/lecciones/01%20-%20Paso%201.ipynb)
* [Paso 2](http://nbviewer.ipython.org/urls/bitbucket.org/franktoffel/cfd-python-class-es/raw/master/lecciones/02%20-%20Paso%202.ipynb)
* [CFL Condition](http://nbviewer.ipython.org/urls/bitbucket.org/franktoffel/cfd-python-class-es/raw/master/lecciones/03%20-%20CFL%20Condition.ipynb)
* [Paso 3](http://nbviewer.ipython.org/urls/bitbucket.org/franktoffel/cfd-python-class-es/raw/master/lecciones/04%20-%20Paso%203.ipynb)
* [Paso 4](http://nbviewer.ipython.org/urls/bitbucket.org/franktoffel/cfd-python-class-es/raw/master/lecciones/05%20-%20Paso%204.ipynb)
* [Operaciones matriciales (arrays) con NumPy](http://nbviewer.ipython.org/urls/bitbucket.org/franktoffel/cfd-python-class-es/raw/master/lecciones/06%20-%20Operaciones%20matriciales%20%28arrays%29%20con%20NumPy.ipynb)
* [Paso 5](http://nbviewer.ipython.org/urls/bitbucket.org/franktoffel/cfd-python-class-es/raw/master/lecciones/07%20-%20Paso%205.ipynb)
* [Paso 6](http://nbviewer.ipython.org/urls/bitbucket.org/franktoffel/cfd-python-class-es/raw/master/lecciones/08%20-%20Paso%206.ipynb)
* [Paso 7](http://nbviewer.ipython.org/urls/bitbucket.org/franktoffel/cfd-python-class-es/raw/master/lecciones/09%20-%20Paso%207.ipynb)
* [Paso 8](http://nbviewer.ipython.org/urls/bitbucket.org/franktoffel/cfd-python-class-es/raw/master/lecciones/10%20-%20Paso%208.ipynb)
* [Definiendo funciones](http://nbviewer.ipython.org/urls/bitbucket.org/franktoffel/cfd-python-class-es/raw/master/lecciones/11%20-%20Definiendo%20funciones.ipynb)
* [Paso 9](http://nbviewer.ipython.org/urls/bitbucket.org/franktoffel/cfd-python-class-es/raw/master/lecciones/12%20-%20Paso%209.ipynb)
* [Paso 10](http://nbviewer.ipython.org/urls/bitbucket.org/franktoffel/cfd-python-class-es/raw/master/lecciones/13%20-%20Paso%2010.ipynb)
* [Optimizando bucles con Numba](http://nbviewer.ipython.org/urls/bitbucket.org/franktoffel/cfd-python-class-es/raw/master/lecciones/14%20-%20Optimizando%20bucles%20con%20Numba.ipynb)
* [Paso 11](http://nbviewer.ipython.org/urls/bitbucket.org/franktoffel/cfd-python-class-es/raw/master/lecciones/15%20-%20Paso%2011.ipynb)
* [Paso 12](http://nbviewer.ipython.org/urls/bitbucket.org/franktoffel/cfd-python-class-es/raw/master/lecciones/16%20-%20Paso%2012.ipynb)